﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VowelsCounter
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private void btnVCount_Click(object sender, EventArgs e)
        {

            var Vowels = new HashSet<char> { 'a', 'e', 'i', 'o', 'u' }; //declaration of constants 

            char[] vowelsChar = textBox1.Text.ToCharArray();

            string temp = new string(vowelsChar).ToLower();

            int vowelsCount = 0;

            for (int i = 0; i < temp.Length; i++) 
            {
                if (Vowels.Contains(temp[i]))
                {
                    vowelsCount++;
                }
            }
            lblVowelCount.Text = "Total Number of Vowels is " + vowelsCount;
        }
    }
}
