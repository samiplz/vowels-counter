﻿namespace VowelsCounter
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btnVCount = new System.Windows.Forms.Button();
            this.lblVowelCount = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(44, 47);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(193, 20);
            this.textBox1.TabIndex = 0;
            // 
            // btnVCount
            // 
            this.btnVCount.Location = new System.Drawing.Point(44, 89);
            this.btnVCount.Name = "btnVCount";
            this.btnVCount.Size = new System.Drawing.Size(193, 23);
            this.btnVCount.TabIndex = 1;
            this.btnVCount.Text = "Count Vowel";
            this.btnVCount.UseVisualStyleBackColor = true;
            this.btnVCount.Click += new System.EventHandler(this.btnVCount_Click);
            // 
            // lblVowelCount
            // 
            this.lblVowelCount.AutoSize = true;
            this.lblVowelCount.Location = new System.Drawing.Point(41, 135);
            this.lblVowelCount.Name = "lblVowelCount";
            this.lblVowelCount.Size = new System.Drawing.Size(99, 13);
            this.lblVowelCount.TabIndex = 2;
            this.lblVowelCount.Text = "Total No Of Vowels";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.lblVowelCount);
            this.Controls.Add(this.btnVCount);
            this.Controls.Add(this.textBox1);
            this.Name = "Form1";
            this.Text = "Count Vowels";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btnVCount;
        private System.Windows.Forms.Label lblVowelCount;
    }
}

